#pragma once

#define ConsoleW 120
#define ConsoleH 30


class Screen
{
public:
	static Screen* GetInstance();
	static void Initialize();
	static void Clear();
	static void Print();
	static void Render();

	static void DrawSprite(GameObject* go);
	static void DrawSprite(char* src, int x, int y, int w, int h);
	static void DrawUI(GameObject* go);
	static void DrawFrame(int x, int y, int w, int h);
	static void DrawName(GameObject* go);

private:
	char buffer[ConsoleH][ConsoleW] = { 0, };
};

