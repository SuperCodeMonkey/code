#include "stdafx.h"
#include "TextBox.h"

TextBox* textBox = nullptr;

TextBox::TextBox()
{
	Initialize("TEXT_BOX");
	ObjectManager::PushUI(this);
}

TextBox::~TextBox()
{
}

void TextBox::Initialize(const char* _name)
{
	this->name = (char*)_name;
	this->w = 118;
	this->h = 9;
	this->y = 20;
	this->x = 1;
	this->textX = x + 2;
	this->textY = y + 2;
	this->isEnable = false;
	this->isVisible = false;
	this->showName = false;
	MakeFrame();
}

void TextBox::Release()
{
}

void TextBox::Render()
{
	if (!isVisible) return;

	Screen::DrawUI(this);
	Screen::DrawSprite(text, textX, textY, strlen(text), h);
}

void TextBox::Update()
{
	if (!isEnable) return;

	if (Input::GetKeyDown(VK_SPACE))
	{
		if (strcmp(text, wait) == 0) // 모두 출력시
		{
			char* str = nullptr;
			if (textQ.Dequeue(&str)) // 대기 문자열 뽑기
			{
				SetText(str); // 있으면 대기문자열 교체
			}
			else
			{
				memset(text, 0, 100); // 없으면 클리어 후에 대화창 숨김
				memset(wait, 0, 100);
				Hide();
			}

			write = 0;
			count = 0;
			return;
		}

		TextBox::Skip(); // 대화 출력 스킵
		return;
	}

	if (strcmp(text, wait) == 0)
	{
		return;
	}

	// 출력대기를 한글자씩 옮겨라
	memcpy(text, wait, write);

	count++;

	if (count >= delay)
	{
		write += speed;
		count = 0;
	}
}


TextBox* TextBox::GetInstance()
{
	if (textBox == nullptr)
	{
		textBox = new TextBox();
	}
	return textBox;
}

void TextBox::Show()
{
	textBox->isVisible = true;
	textBox->isEnable = true;
}

void TextBox::Show(const char* _text)
{
	textBox->isVisible = true;
	textBox->isEnable = true;
	textBox->EnqueueText(_text);
}

void TextBox::Hide()
{
	textBox->isVisible = false;
	textBox->isEnable = false;
}

void TextBox::Skip()
{
	textBox->write = strlen(textBox->wait);
	memcpy(textBox->text, textBox->wait, textBox->write);
}

void TextBox::StartMessage()
{
	ObjectManager::Pause();
	textBox->isEnd = false;
}

void TextBox::EndMessage()
{
	textBox->EnqueueText("End");
}

bool TextBox::IsEnd()
{
	return textBox->isEnd;
}

void TextBox::ShowCondition()
{
	textBox->EnqueueText("Condition");
}


void TextBox::EnqueueText(const char * _text)
{
	textQ.Enqueue((char*)_text);
}

void TextBox::SetText(const char * _text)
{
	memset(text, 0, 100);
	memset(wait, 0, 100);

	if (strcmp(_text, "End") == 0)
	{
		isEnd = true;
		ObjectManager::Resume();
		Hide();
		return;
	}
	if (strcmp(_text, "Condition") == 0)
	{
		Input::Clear();
		ConditionBox::Show();
		return;
	}
	
	unsigned int len = min(strlen(_text), 95);
	memcpy(wait, _text, len);
}

void TextBox::SetType(UI_TYPE type)
{
}