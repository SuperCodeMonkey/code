#include "stdafx.h"
#include "UI.h"


UI::UI()
{
}

UI::~UI()
{
}

void UI::Initialize()
{
	
}

void UI::Release()
{
}

void UI::Render()
{
	
}

void UI::Update()
{
	
}

void UI::MakeFrame()
{
	// 예외처리
	if (this->shape != nullptr) return;
	if (this->w == 0) return;
	if (this->h == 0) return;
	// 프레임 버퍼 생성
	unsigned int bufferSize = w * h;
	this->shape = new char[bufferSize];

	for (int row = 0; row < h; row++)
	{
		for (int col = 0; col < w; col++)
		{
			shape[row * w + col] = ' ';

			if (row == 0 || row == h-1 || col == 0 || col == w-1)
			{
				shape[row * w + col] = '@';
			}
		}
	}
}
