#pragma once
#include <vector>
#include "GameObject.h"
#include "Player.h"
#include "Monster.h"
#include "UI.h"
#include "TextBox.h"
#include "NPC.h"
#include "Queue.h"
#include "Menu.h"

using namespace std;

enum class ObjectType : char
{
	PLAYER,
	MONSTER,
	NON_PLAYER_CHARACTER,
	TEXT_BOX,

};

class ObjectManager
{
private:
	ObjectManager();
	~ObjectManager();

public:
	static ObjectManager* GetInstance();
	static void Update();
	static void Render();
	static void Pause();
	static void Pause(GameObject* target);
	static void Resume();
	static void Resume(GameObject* target);

	static GameObject* CreateObject(ObjectType type, const char* _name);
	static void DeleteObject(GameObject* target);
	static bool FindObject(GameObject* target);
	static bool FindUI(GameObject* target);
	static void PushObject(GameObject* target);
	static void PushUI(GameObject* target);

	static GameObject* GetNearbyObject(GameObject* ref);

private:
	bool isPause = false;
	vector<GameObject*> list;
	vector<GameObject*> uiList;
	Queue<GameObject*> waitingQueue;
	Queue<GameObject*> uiWaitingQueue;
	map<GameObject*, bool> enableStorage;
};

