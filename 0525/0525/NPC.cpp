#include "stdafx.h"
#include "NPC.h"

NPC* npc = nullptr;

NPC::NPC()
{
	Initialize("npc");
	ObjectManager::PushObject(this);
}

NPC::~NPC()
{
}

void NPC::Initialize(const char* _name)
{
	this->shape = (char*)npcShape;
	this->name = (char*)_name;
	this->w = 4;
	this->h = 3;
	this->x = 30;
	this->y = 15;
	this->isEnable = true;
	this->isVisible = true;
	this->showName = true;
	this->event = this->Event_Start;
}

void NPC::Release()
{
}

void NPC::Render()
{
	if (!isVisible) return;

	Screen::DrawName(this);
	Screen::DrawSprite(this);
}

void NPC::Update()
{
	if (!isEnable) return;

	RandomWalk(30, 15, 3);
}

NPC* NPC::GetInstance()
{
	if (npc == nullptr)
	{
		npc = new NPC();
	}
	return npc;
}

void NPC::RandomWalk(int x, int y, int dist)
{
	if (IsWalkCoolTime()) return;

	int direction = rand() % 4;
	int modifierX = this->x;
	int modifierY = this->y;

	switch (direction)
	{
	case 0: // up
		modifierY--;
		break;
	case 1: // down
		modifierY++;
		break;
	case 2: // left
		modifierX--;
		break;
	case 3: // right
		modifierX++;
		break;
	default:
		break;
	}

	if (abs(x - modifierX) > dist) return;
	if (abs(y - modifierY) > dist) return;

	this->x = modifierX;
	this->y = modifierY;

}

bool NPC::IsWalkCoolTime()
{
	DWORD curTime = timeGetTime();

	if (lastWalkTime == 0)
	{
		lastWalkTime = curTime;
		return false;
	}

	DWORD time = curTime - lastWalkTime;

	if (time < 200)
	{
		return true;
	}

	lastWalkTime = curTime;

	return false;
}



void NPC::Event_Start()
{
	TextBox::StartMessage();
	TextBox::Show("안녕하십니까. 저는 NPC 입니다");
	TextBox::Show("이 게임은 직업을 골라야만 다음 내용으로 진행할 수 있습니다.");
	TextBox::EndMessage();
	
	npc->SetEvent(Event_SelectJob);
}

void NPC::Event_SelectJob()
{
	if (!TextBox::IsEnd()) return;

	TextBox::StartMessage();
	
	TextBox::Show("직업을 고르세요.");
	ConditionBox::SetCondition("전사");
	ConditionBox::SetCondition("마법사");
	ConditionBox::SetCondition("도적");
	TextBox::ShowCondition();
	TextBox::EndMessage();

	npc->SetEvent(Event_GoFight);
}

void NPC::Event_GoFight()
{
	int selection = ConditionBox::GetSelectionIndex();
	
	switch (selection)
	{
	case 0:
		TextBox::Show("당신은 [전사]를 고르셨습니다.");
		break;
	case 1:
		TextBox::Show("당신은 [마법사]를 고르셨습니다.");
		break;
	case 2:
		TextBox::Show("당신은 [도적]을 고르셨습니다.");
		break;
	default:
		break;
	}

	npc->SetEvent(Event_End);
}

void NPC::Event_End()
{
	TextBox::StartMessage();
	TextBox::Show("이제 당신은 던전으로 가서 보스몹을 처치하시기 바랍니다.");
	TextBox::EndMessage();
}



