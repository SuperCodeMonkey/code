#pragma once
#include "GameObject.h"


struct Monster : public GameObject
{
	virtual void Initialize(const char* _name);
	virtual void Release();
	virtual void Render();
	virtual void Update();


	char name[32] = { 0, };
	int hp = 0;
	int maxHp = 0;
	int offense = 0;
	int defense = 0;
};

