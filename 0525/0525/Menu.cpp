#include "stdafx.h"
#include "Menu.h"

Menu* menu = nullptr;

Menu::Menu()
{
	Initialize("MENU");
	ObjectManager::PushUI(this);
}

Menu::~Menu()
{
}

void Menu::Initialize(const char* _name)
{
	this->name = (char*)_name;
	this->w = 40;
	this->h = 30;
	this->x = 0;
	this->y = 0;

	this->isEnable = true;
	this->isVisible = false;
	this->showName = false;
	MakeFrame();
}

void Menu::Release()
{
}

void Menu::Render()
{
	if (!isVisible) return;

	int titleX = x + (w / 2) - strlen(name);
	int titleY = y + 2;

	Screen::DrawUI(this);
	Screen::DrawSprite(name, titleX, titleY, strlen(name), 1);
}

void Menu::Update()
{
	if (!isEnable) return;

	if (Input::GetKeyDown(VK_ESCAPE))
	{
		ShowHide();
	}
}

Menu* Menu::GetInstance()
{
	if (menu == nullptr)
	{
		menu = new Menu();
	}
	return menu;
}

void Menu::Show()
{
	menu->isVisible = true;
	ObjectManager::Pause();
}

void Menu::Show(const char* _text)
{
	menu->isVisible = true;
	ObjectManager::Pause();
}

void Menu::Hide()
{
	menu->isVisible = false;
}

void Menu::ShowHide()
{
	menu->isVisible = !menu->isVisible;

	if (menu->isVisible)
	{
		ObjectManager::Pause();
	}
	else
	{
		ObjectManager::Resume();
	}
}
