#pragma once
#include "GameObject.h"
#include "Queue.h"

using namespace std;


enum UI_TYPE
{
	
};

class UI : public GameObject
{
public:

	UI();
	virtual ~UI();

	virtual void Initialize();
	virtual void Release();
	virtual void Render();
	virtual void Update();
	
	void MakeFrame();
};
