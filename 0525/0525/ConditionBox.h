#pragma once
#include "UI.h"

#define dfMaxSelectionCount 4


class ConditionBox : public UI
{
private:
	ConditionBox();
	~ConditionBox();
public:
	virtual void Initialize(const char* _name);
	virtual void Release();
	virtual void Render();
	virtual void Update();

	static ConditionBox* GetInstance();
	static void Show();
	static void Hide();
	static void SetCondition(const char* _conTitle);
	static int GetSelectionIndex();

private:
	vector<char*> selection;
	int selectionIndex = 0;
};

