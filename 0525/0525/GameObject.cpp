#include "stdafx.h"
#include "GameObject.h"



void GameObject::Event()
{
	if (event != nullptr)
	{
		this->event();
	}
	
}

bool GameObject::IsTriggeredEvent(DWORD _delay)
{
	currEventTime = timeGetTime();

	if (lastEventTime == 0)
	{
		lastEventTime = currEventTime;
		return false;
	}

	DWORD time = currEventTime - lastEventTime;

	if (time < _delay) return true;

	lastEventTime = currEventTime;

	return false;
}

void GameObject::SetEvent(void(*_event)())
{
	if (_event == nullptr) return;
	event = _event;
}
