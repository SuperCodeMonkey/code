#pragma once
#include "GameObject.h"
#include <Windows.h>

class NPC : public GameObject
{
private:
	NPC();
	~NPC();
public:
	virtual void Initialize(const char* _name);
	virtual void Release();
	virtual void Render();
	virtual void Update();

	static NPC* GetInstance();

	void RandomWalk(int x, int y, int dist);
	bool IsWalkCoolTime();

private:
	static void Event_Start();
	static void Event_SelectJob();
	static void Event_GoFight();
	static void Event_End();

	DWORD lastWalkTime = 0;

	char npcShape[3][4] =
	{
		{' ','<','>',' '},
		{'!','[',']','!'},
		{' ','!','!',' '},
	};
};

