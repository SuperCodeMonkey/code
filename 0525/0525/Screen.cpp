#include "stdafx.h"
#include "Screen.h"

static Screen screen;

Screen* Screen::GetInstance()
{
	return &screen;
}

void Screen::Initialize()
{
	Clear();
}

void Screen::Clear()
{
	int i, j;
	for (i = 0; i < ConsoleH; i++)
	{
		for (j = 0; j < ConsoleW; j++)
		{
			if (j == ConsoleW - 1)
			{
				screen.buffer[i][j] = '\n';
			}

			screen.buffer[i][j] = ' ';
		}
	}
	screen.buffer[i-1][j-1] = 0;
}

void Screen::Print()
{
	printf("%s", screen.buffer);
}

void Screen::Render()
{
	Screen::Clear();
	ObjectManager::Render();
	ConsoleCursor::Move(0, 0);
	Screen::Print();
}

void Screen::DrawSprite(GameObject* go)
{
	if (!go->isVisible) return;
	if (go->shape == nullptr) return;

	char pixel;

	for (int row = 0, i = go->y; row < go->h; row++, i++)
	{
		if (i < 0) continue;
		if (i >= ConsoleH) continue;
		for (int col = 0, j = go->x; col < go->w; col++, j++)
		{
			if (j < 0) continue;
			if (j >= ConsoleW) continue;

			pixel = go->shape[row * go->w + col];

			if (pixel == '\0')continue;
			if (pixel == ' ') continue;
			screen.buffer[i][j] = pixel;
		}
	}
}

void Screen::DrawSprite(char * src, int x, int y, int w, int h)
{
	if (src == nullptr) return;

	char pixel;

	for (int row = 0, i = y; row < h; row++, i++)
	{
		if (i < 0) continue;
		if (i >= ConsoleH) continue;
		for (int col = 0, j = x; col < w; col++, j++)
		{
			if (j < 0) continue;
			if (j >= ConsoleW) continue;
			pixel = src[row * w + col];
			if (pixel == ' ') continue;

			screen.buffer[i][j] = pixel;
		}
	}
}

void Screen::DrawUI(GameObject* go)
{
	if (!go->isVisible) return;
	if (go->shape == nullptr) return;

	char pixel;

	for (int row = 0, i = go->y; row < go->h; row++, i++)
	{
		if (i < 0) continue;
		if (i >= ConsoleH) continue;
		for (int col = 0, j = go->x; col < go->w; col++, j++)
		{
			if (j < 0) continue;
			if (j >= ConsoleW) continue;

			pixel = go->shape[row * go->w + col];

			if (pixel == '\0')return;
			screen.buffer[i][j] = pixel;
		}
	}
}

void Screen::DrawFrame(int x, int y, int w, int h)
{

	for (int row = 0, i = y; row < h; row++, i++)
	{
		if (i < 0) continue;
		if (i >= ConsoleH) continue;
		for (int col = 0, j = x; col < w; col++, j++)
		{
			if (j < 0) continue;
			if (j >= ConsoleW) continue;

			if (row == 0 || row == h-1 || col ==0 || col == w-1)
			{
				screen.buffer[i][j] = '@';
			}

			
		}
	}
}

void Screen::DrawName(GameObject* go)
{
	if (go->name == nullptr) return;

	int horizonCenter = go->x + (go->w / 2);
	int halfLength = strlen(go->name) / 2;
	int namePosition = horizonCenter - halfLength;

	DrawSprite(go->name, namePosition, go->y - 2, strlen(go->name), 1);
}

