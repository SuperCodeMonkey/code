#pragma once
#include "GameObject.h"

enum class KeyCode : int
{
	None =0,

};

class Input
{
private:
	Input();
	~Input();

	void ClearKey();
	void ClearKeyDown();
	void SetKey();

public:
	static Input* GetInstance();
	static void Update();
	static void Clear();
	static bool GetKey(int vKey);
	static bool GetKeyDown(int vKey);
	static void SetFocus(GameObject* _focusObject);


private:
	GameObject* focusObject = nullptr;
	bool keys[256] = { 0, };
	bool keyDowns[256] = { 0 };

};

