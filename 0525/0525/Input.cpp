#include "stdafx.h"
#include "Input.h"

Input* input;

Input::Input()
{

}

Input::~Input()
{
	delete input;
}

void Input::ClearKey()
{
	memset(keys, 0, 256);
}

void Input::ClearKeyDown()
{
	memset(keyDowns, 0, 256);
}

void Input::SetKey()
{

	SHORT state;

	for (int i = 0; i < 256; i++)
	{
		state = GetAsyncKeyState(i);

		if( state & 0x0001)
		{
			keys[i] = true;
			keyDowns[i] = true;
			continue;
		}
		else if (state)
		{
			keys[i] = true;
			keyDowns[i] = false;
			continue;
		}

		keyDowns[i] = false;
		keys[i] = false;
		
	}
}

Input* Input::GetInstance()
{
	if (input == nullptr)
	{
		input = new Input();
	}
	

	return input;
}

void Input::Update()
{
	input->SetKey();
	
}

void Input::Clear()
{
	input->ClearKey();
	input->ClearKeyDown();
}

bool Input::GetKey(int vKey)
{
	if (input->focusObject != nullptr)
	{

	}
	return input->keys[vKey];
}

bool Input::GetKeyDown(int vKey)
{
	return input->keyDowns[vKey];
}

void Input::SetFocus(GameObject * _focusObject)
{
	input->focusObject = _focusObject;
}
