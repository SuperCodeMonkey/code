#pragma once
#include <Windows.h>

class GameObject
{
public:
	virtual void Initialize(const char* _name) = 0;
	virtual void Release() = 0;
	virtual void Render() = 0;
	virtual void Update() = 0;
	void Event();

	bool IsTriggeredEvent(DWORD _delay);
	void SetEvent(void(*_event)());

	int x = 0;
	int y = 0;
	int w = 0;
	int h = 0;
	char* shape = nullptr;
	char* name = nullptr;
	bool isEnable = false;
	bool isVisible = false;
	bool showName = false;
	
protected:
	void (*event)();
private:
	
	DWORD lastEventTime = 0;
	DWORD currEventTime = 0;
};

