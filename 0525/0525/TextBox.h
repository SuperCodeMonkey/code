#pragma once
#include "UI.h"



class TextBox : public UI
{
private:
	TextBox();
	~TextBox();
	
public:
	virtual void Initialize(const char* _name);
	virtual void Release();
	virtual void Render();
	virtual void Update();

	static TextBox* GetInstance();
	static void Show();
	static void Show(const char* _text);
	static void Hide();
	static void Skip();
	static void StartMessage();
	static void EndMessage();
	static bool IsEnd();
	static void ShowCondition();
private:
	void EnqueueText(const char* _text);
	void SetText(const char* _text);
	void SetType(UI_TYPE type);



private:

	// 출력 텍스트 관련
	bool isEnd = true;
	char text[100] = { 0, }; // 출력용 텍스트
	char wait[100] = { 0, }; // 출력 대기 (한글자씩 출력용으로 옮길거)
	int write = 0; // 출력으로 옮긴 글자수
	int delay = 4; // delay 프레임 당 speed 만큼 출력
	int speed = 2;
	int count = 0; // Update 호출횟수 / delay 랑 상호작용
	int textX = 0;
	int textY = 0;
	Queue<char*> textQ; // 대화창 대기열

};

