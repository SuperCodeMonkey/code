#include "stdafx.h"
#include "Player.h"

Player* player = nullptr;

Player::Player()
{
	Initialize("player");
	ObjectManager::PushObject(this);
}

Player::~Player()
{
}

void Player::Initialize(const char* _name)
{
	this->shape = (char*)playerShape;
	this->name = (char*)_name;
	this->w = 4;
	this->h = 3;
	this->x = 3;
	this->y = 3;
	this->isEnable = true;
	this->isVisible = true;
}

void Player::Release()
{
}

void Player::Render()
{
	if (!isVisible) return;

	Screen::DrawName(this);
	Screen::DrawSprite(this);
}

void Player::Update()
{
	if (!isEnable) return;

	if (Input::GetKey(VK_UP))
	{
		this->y--;
	}
	if (Input::GetKey(VK_DOWN))
	{
		this->y++;
	}
	if (Input::GetKey(VK_RIGHT))
	{
		this->x++;
	}
	if (Input::GetKey(VK_LEFT))
	{
		this->x--;
	}
	if (Input::GetKeyDown(VK_SPACE))
	{
		GameObject* target = ObjectManager::GetNearbyObject(this);
		if (target != nullptr)
		{
			target->Event();
		}
	}
}

Player* Player::GetInstance()
{
	if (player == nullptr)
	{
		player = new Player();
	}

	return player;
}



