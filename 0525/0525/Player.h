#pragma once
#include "GameObject.h"

enum Job
{
	None = 0,
	Warrior,
	Wizard,
	Thief,
};

class Player : public GameObject
{
private:
	Player();
	~Player();
public:
	virtual void Initialize(const char* _name);
	virtual void Release();
	virtual void Render();
	virtual void Update();

	static Player* GetInstance();
	
private:
	Job job = Job::None;
	int level = 0;
	int hp = 0;
	int maxHp = 0;
	int offense = 0;
	int defense = 0;

	char playerShape[3][4] =
	{
		{' ','(',')',' '},
		{'!','(',')','!'},
		{' ','!','!',' '},
	};
};

