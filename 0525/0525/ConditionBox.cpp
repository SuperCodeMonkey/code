#include "stdafx.h"
#include "ConditionBox.h"

ConditionBox* conBox = nullptr;

ConditionBox::ConditionBox()
{
	Initialize("CONDITION_BOX");
	ObjectManager::PushUI(this);
}

ConditionBox::~ConditionBox()
{
}

void ConditionBox::Initialize(const char* _name)
{
	this->shape = nullptr;
	this->name = (char*)_name;
	this->w = 118;
	this->h = 9;
	this->y = 25;
	this->x = 5;
	this->isEnable = false;
	this->isVisible = false;
	this->showName = false;
}

void ConditionBox::Release()
{
}

void ConditionBox::Render()
{
	if (!isVisible) return;

	int count = selection.size();
	char* selectionText = nullptr;
	int selectionX = 0;
	int selectionW = 0;

	for (int i = 0; i < count; i++)
	{
		selectionText = selection[i];
		if (selectionText == nullptr) return;
		selectionX = x + 5 + (i * 20);
		selectionW = strlen(selectionText);

		if (selectionIndex == i)
		{
			Screen::DrawFrame(selectionX - 2, y-1, selectionW + 4, 3);
		}

		Screen::DrawSprite(selectionText, selectionX, y, selectionW, 1);
	}
}

void ConditionBox::Update()
{
	if (!isEnable) return;


	int count = selection.size();
	int index = 0;
	if (Input::GetKeyDown(VK_SPACE))
	{
		Event();
		Hide();
	}
	if (Input::GetKeyDown(VK_LEFT))
	{
		index = abs(selectionIndex - 1);
		selectionIndex = (index == 0) ? index : index % count;
	}
	if (Input::GetKeyDown(VK_RIGHT))
	{
		index = abs(selectionIndex + 1);
		selectionIndex = (index == 0) ? index : index % count;
	}
}


ConditionBox* ConditionBox::GetInstance()
{
	if (conBox == nullptr)
	{
		conBox = new ConditionBox();
	}
	return conBox;
}

void ConditionBox::Show()
{
	ObjectManager::Pause(TextBox::GetInstance());
	conBox->isVisible = true;
	conBox->isEnable = true;
	conBox->selectionIndex = 0;
	
}

void ConditionBox::Hide()
{
	ObjectManager::Resume(TextBox::GetInstance());
	conBox->isVisible = false;
	conBox->isEnable = false;
	conBox->selection.clear();
}

void ConditionBox::SetCondition(const char* _conTitle)
{
	if (conBox->selection.size() >= dfMaxSelectionCount) return;

	conBox->selection.push_back((char*)_conTitle);
}

int ConditionBox::GetSelectionIndex()
{
	return conBox->selectionIndex;
}
