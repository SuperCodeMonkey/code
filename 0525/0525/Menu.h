#pragma once
#include "UI.h"


class Menu : public UI
{
private:
	Menu();
	~Menu();

public:
	virtual void Initialize(const char* _name);
	virtual void Release();
	virtual void Render();
	virtual void Update();

	static Menu* GetInstance();
	static void Show();
	static void Show(const char* _text);
	static void Hide();
	static void ShowHide();
};

