#pragma once

#pragma comment(lib,"winmm.lib")


#include <iostream>
#include <vector>
#include <Windows.h>
#include <direct.h>
#include <string>
#include <map>

#include "GameObject.h"
#include "GameData.h"
#include "Screen.h"
#include "ConsoleCursor.h"
#include "ObjcetManager.h"
#include "Input.h"
#include "ConditionBox.h"


using namespace std;